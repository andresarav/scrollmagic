Hola,

// y si instalo gsap, scroll magic y imports-loader

npm install gsap
npm install scrollmagic
npm install imports-loader // this is important

// Importar la definición de TypeScript desde aquí https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/gsap
npm install @types/gsap


// crear un Servicio


// ##App.servise.ts########################################################################################################################
// ###########################################################################################################################################


import { Injectable } from '@angular/core';
import Scroll from "scrollmagic";

@Injectable({
  providedIn: 'root'
})
export class AppState {
public ScrollMagic : any;
 public controller :any;

  constructor() {
    this.ScrollMagic = Scroll;
    // require("scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap");
    this.controller = new this.ScrollMagic.Controller();
   }
}


// ##End########################################################################################################################
// ###########################################################################################################################################



en el componente
// ##home.component.ts########################################################################################################################
// ###########################################################################################################################################



import { Component, OnInit, ElementRef, ViewChild  } from '@angular/core';
import { AppState } from '../app.service';
import { TweenMax, TimelineMax } from "gsap";

export class HomeComponent implements OnInit {

@ViewChild("test") test: ElementRef;
  constructor(private appState: AppState) {}

   ngOnInit(){
    console.log("hola", this.appState);

    var scene = new this.appState.ScrollMagic.Scene({
    duration: 200,
    offset: 50
  })
    .setPin("#trigger") // pins the element for the the scene's duration
    .addTo(this.appState.controller);

  }
}

// ##End########################################################################################################################
// ###########################################################################################################################################
